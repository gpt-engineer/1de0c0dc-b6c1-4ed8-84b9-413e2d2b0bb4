document.getElementById('task-form').addEventListener('submit', function(event) {
    event.preventDefault();

    const taskInput = document.getElementById('task-input');
    const taskList = document.getElementById('task-list');

    if (taskInput.value.trim() === '') {
        alert('Please enter a task');
        return;
    }

    const taskElement = document.createElement('li');
    taskElement.textContent = taskInput.value;
    taskElement.classList.add('px-4', 'py-2', 'bg-white', 'shadow', 'flex', 'justify-between', 'items-center');

    const doneButton = document.createElement('button');
    doneButton.innerHTML = '<i class="fas fa-check text-green-500"></i>';
    doneButton.addEventListener('click', function() {
        taskElement.classList.add('line-through', 'text-gray-500');
    });

    const deleteButton = document.createElement('button');
    deleteButton.innerHTML = '<i class="fas fa-trash text-red-500"></i>';
    deleteButton.addEventListener('click', function() {
        taskList.removeChild(taskElement);
    });

    taskElement.appendChild(doneButton);
    taskElement.appendChild(deleteButton);

    taskList.appendChild(taskElement);

    taskInput.value = '';
});
